#include "stdio.h"
// #include <linux/i2c-dev.h>
#include "linux/i2c-dev.h"
#include "linux/i2c.h"
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "/usr/include/i2c/smbus.h"
#include "/usr/include/asm-generic/fcntl.h"
#include "/home/adam/yocto/ros-bmi160/BMI160_driver/bmi160.h"

/*****************************************************************************/
/*!                         Global variables                                 */
int fd;
static const char I2C_PERYFERIUM_NAME[] = "/dev/i2c-2"; //i2c bus name

/****************************************************************************/
/*!                         Functions                                       */

void user_delay_ms(uint32_t period);

//void print_sensor_data(struct bme280_data *comp_data);

/*!
 *  @brief Function for reading the sensor's registers through I2C bus.
 *
 *  @param[in] id       : Sensor I2C address. /dev/i2c-2
 *  @param[in] reg_addr : Register address.   0x68
 *  @param[out] data    : Pointer to the data buffer to store the read data.
 *  @param[in] len      : No of bytes to read.
 *
 *  @return Status of execution
 *
 *  @retval 0 -> Success
 *  @retval > 0 -> Failure Info
 *
 */
int8_t user_i2c_read(uint8_t id, uint8_t reg_addr, uint8_t *data, uint16_t len);

/*!
 *  @brief Function for writing the sensor's registers through I2C bus.
 *
 *  @param[in] id       : Sensor I2C address.
 *  @param[in] reg_addr : Register address.
 *  @param[in] data     : Pointer to the data buffer whose value is to be written.
 *  @param[in] len      : No of bytes to write.
 *
 *  @return Status of execution
 *
 *  @retval BMI160_OK -> Success
 *  @retval BMI160_E_COM_FAIL -> Communication failure.
 *
 */
int8_t user_i2c_write(uint8_t id, uint8_t reg_addr, uint8_t *data, uint16_t len);

/*!
 * @brief Function reads temperature, humidity and pressure data in forced mode.
 *
 * @param[in] dev   :   Structure instance of bme280_dev.
 *
 * @return Result of API execution status
 *
 * @retval BME280_OK - Success.
 * @retval BMI160_E_NULL_PTR - Error: Null pointer error
 * @retval BMI160_E_COM_FAIL - Error: Communication fail error
 *
 */
//int8_t stream_sensor_data_forced_mode(struct bme280_dev *dev);

int main() //args are I2C_PERYFERIUM_NAME ,
{
    struct bmi160_dev dev;

    /* Variable to define the result */
    int8_t rslt = BMI160_OK;

    dev.id = BMI160_I2C_ADDR;
    dev.interface = BMI160_I2C_INTF;
    dev.read = user_i2c_read;
    dev.write = user_i2c_write;
    dev.delay_ms = user_delay_ms;

    if ((fd = open(I2C_PERYFERIUM_NAME, O_RDWR)) < 0)
    {
        fprintf(stderr, "Failed to open the i2c bus %s\n", I2C_PERYFERIUM_NAME);
        exit(1);
    }

    /* Initialize the bmi160 */
    rslt = bmi160_init(&dev);
    while(rslt != BMI160_OK)
    {
        fprintf(stderr, "Failed to initialize the device (code %+d).\n", rslt);
        dev.delay_ms(1000);
        //exit(1);
    }

    /* Setup imu */

    dev.accel_cfg.odr = BMI160_ACCEL_ODR_1600HZ;
    dev.accel_cfg.range = BMI160_ACCEL_RANGE_2G;
    dev.accel_cfg.bw = BMI160_ACCEL_BW_NORMAL_AVG4;

    /* Select the power mode of accelerometer sensor */
    dev.accel_cfg.power = BMI160_ACCEL_NORMAL_MODE;

    /* Select the Output data rate, range of Gyroscope sensor */
    dev.gyro_cfg.odr = BMI160_GYRO_ODR_3200HZ;
    dev.gyro_cfg.range = BMI160_GYRO_RANGE_2000_DPS;
    dev.gyro_cfg.bw = BMI160_GYRO_BW_NORMAL_MODE;

    /* Select the power mode of Gyroscope sensor */
    dev.gyro_cfg.power = BMI160_GYRO_NORMAL_MODE;

    /* Set the sensor configuration */
    rslt = bmi160_set_sens_conf(&dev);

    while (1)
    {
        int8_t rslt = BMI160_OK;
        struct bmi160_sensor_data accel;
        struct bmi160_sensor_data gyro;

        /* To read only Accel data */
        rslt = bmi160_get_sensor_data(BMI160_ACCEL_SEL, &accel, NULL, &dev);
        dev.delay_ms(1000);
        printf("x: %f\n",accel.x);
    }

    return 0;
}

/*!
 * @brief This function reading the sensor's registers through I2C bus.
 */
int8_t user_i2c_read(uint8_t id, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
    write(fd, &reg_addr, 1);
    read(fd, data, len);

    return 0;
}

/*!
 * @brief This function provides the delay for required time (Microseconds) as per the input provided in some of the
 * APIs
 */
void user_delay_ms(uint32_t period)
{
    /* Milliseconds convert to microseconds */
    usleep(period * 1000);
}

/*!
 * @brief This function for writing the sensor's registers through I2C bus.
 */
int8_t user_i2c_write(uint8_t id, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
    int8_t *buf;

    buf = malloc(len + 1);
    buf[0] = reg_addr;
    memcpy(buf + 1, data, len);
    if (write(fd, buf, len + 1) < len)
    {
        return 1;
    }

    free(buf);

    return BMI160_OK;
}
