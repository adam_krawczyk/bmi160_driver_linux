#include "stdio.h"
// #include <linux/i2c-dev.h>
#include "linux/i2c-dev.h"
#include "linux/i2c.h"
#include "/usr/include/i2c/smbus.h"
#include "/usr/include/asm-generic/fcntl.h"
#include "/home/adam/yocto/ros-bmi160/BMI160_driver/bmi160.h"

static const char I2C_PERYFERIUM_NAME[] = "/dev/i2c-2"; //i2c bus name

int8_t rslt = BMI160_OK;
struct bmi160_sensor_data accel;
struct bmi160_sensor_data gyro;
int file;

void init_file()
{   
    file = open(I2C_PERYFERIUM_NAME, O_RDWR);
    if (file < 0)
    {
        /* ERROR HANDLING; you can check errno to see what went wrong */
        printf("error 1 read \n");
        return 1;
    }

    if (ioctl(file, I2C_SLAVE, 0x68) < 0)
    {
        /* ERROR HANDLING; you can check errno to see what went wrong */
        printf("error 2");
        return 1;
    }

    ///* --------------------------------------------------------- *
    //* Set I2C device (BMI160 I2C address is  0x68 or 0x69)      *
    //* --------------------------------------------------------- */
    //int addr = (int)strtol(I2C_SLAVE, NULL, 16);
    int addr = 0x68;
    printf("Debug: Sensor address: [0x%02X]\n", addr);

    if (ioctl(file, I2C_SLAVE, addr) != 0)
    {
        printf("Error can't find sensor at address [0x%02X].\n", addr);
        exit(1);
    }
    ///* --------------------------------------------------------- *
    //* I2C communication test is the only way to confirm success *
    //* --------------------------------------------------------- */
    char reg = 0x64;
    if (write(file, &reg, 1) != 1)
    {
        printf("Error: I2C write failure register [0x%02X], sensor addr [0x%02X]?\n", reg, addr);
        exit(1);
    }
}

int8_t write_fcn(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data, uint16_t len)
{

    char buf[len + 1];
    (void)dev_addr;
    //init_file();
    // dev_addr = "/dev/i2c-%d"

    // int file = open(I2C_PERYFERIUM_NAME, O_RDWR);
    // if (file < 0) {
    //     /* ERROR HANDLING; you can check errno to see what went wrong */
    //     printf("error 1");
    //     return 1;
    // }

    //0x68; /* The I2C address */

    buf[0] = reg_addr;
    for (int i = 0; i < len; i++)
    {
        buf[i + 1] = data[i];
    }

    if (write(file, buf, len + 1) != (len + 1))
    {
        /* ERROR HANDLING: i2c transaction failed */
        printf("error writing \n");
        return 1;
    }
    return 0;
}

int8_t read_fcn(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
    char buf[len + 1];
    buf[0] = reg_addr;

    (void)dev_addr;
    //init_file();
    // int file = open(I2C_PERYFERIUM_NAME, O_RDWR);
    // if (file < 0) {
    //     /* ERROR HANDLING; you can check errno to see what went wrong */
    //     printf("error 1 read \n");
    //     return 1;
    // }

    //0x68; /* The I2C address */

    // if (ioctl(file, I2C_SLAVE, dev_addr) < 0) {
    //     /* ERROR HANDLING; you can check errno to see what went wrong */
    //     printf("error 2");
    //     return 1;
    // }

    if (write(file, buf, 1) != 1)
    {
        /* ERROR HANDLING: i2c transaction failed */
        printf("error writing \n");
        return 1;
    }

    /* Using I2C Read, equivalent of i2c_smbus_read_byte(file) */
    if (read(file, buf, len) != len)
    {
        /* ERROR HANDLING: i2c transaction failed */
        printf("error reading \n");
        return 1;
    }
    else
    {
        /* buf[0] contains the read byte */
        //printf("works \n");
        // printf("0x%02X \n",buf[1],buf[2],buf[3]);
    }
    return 0;
}

void delay_ms_fcn(uint32_t period)
{
    usleep(period * 1000);
}

int main(void)
{
    
    printf("start\n");
    init_file();
    printf("end init");
    struct bmi160_dev sensor;
    
    sensor.id = BMI160_I2C_ADDR;
    sensor.interface = BMI160_I2C_INTF;
    sensor.read = &read_fcn;
    sensor.write = &write_fcn;
    sensor.delay_ms = &delay_ms_fcn;

    int8_t rslt = BMI160_OK;
    printf("rslt = %d \n", rslt);
    rslt = bmi160_init(&sensor);
    printf("rslt = %d \n", rslt);
    while (rslt != 0)
    {
        rslt = bmi160_init(&sensor);
        printf("rslt = %d \n", rslt);
        usleep(100000);
    }

    /* Select the Output data rate, range of accelerometer sensor */
    sensor.accel_cfg.odr = BMI160_ACCEL_ODR_1600HZ;
    sensor.accel_cfg.range = BMI160_ACCEL_RANGE_2G;
    sensor.accel_cfg.bw = BMI160_ACCEL_BW_NORMAL_AVG4;

    /* Select the power mode of accelerometer sensor */
    sensor.accel_cfg.power = BMI160_ACCEL_NORMAL_MODE;

    /* Select the Output data rate, range of Gyroscope sensor */
    sensor.gyro_cfg.odr = BMI160_GYRO_ODR_3200HZ;
    sensor.gyro_cfg.range = BMI160_GYRO_RANGE_2000_DPS;
    sensor.gyro_cfg.bw = BMI160_GYRO_BW_NORMAL_MODE;

    /* Select the power mode of Gyroscope sensor */
    sensor.gyro_cfg.power = BMI160_GYRO_NORMAL_MODE;

    /* Set the sensor configuration */
    rslt = bmi160_set_sens_conf(&sensor);
    printf("rslt = %d \n", rslt);
    usleep(100000);

    while (1)
    {
        rslt = bmi160_get_sensor_data(BMI160_ACCEL_SEL, &accel, NULL, &sensor);
        printf("data: %d, %d, %d ,%d \n", accel.x, accel.y, accel.z, accel.sensortime);
        printf("rslt = %d \n", rslt);
        usleep(100);
    }

    return 0;
}

